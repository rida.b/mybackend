'use strict';

var response = require('./res');
var connection = require('./conn');

exports.index = function (req, res) {
    response.formatResponse("200", "Sucess", "Hello from the Node JS RESTful side!", [], res)
};

exports.getCustomers = function (req, res) {
    connection.query('SELECT * FROM tbl_customer', function (error, rows, fields) {
        if (error) {
            response.formatResponse('400', 'Failed', 'Get Data Failed!', [], res)
        } else {
            response.formatResponse('200', 'Success', 'Get Data Success!', rows, res)
        }
    });
};

exports.postCustomers = function (req, res) {
    var requestBody = req.body;
    var query = "INSERT INTO `db_customer`.`tbl_customer` (`id`, `name`, `email`, `password`, `gender`, `is_married`, `address`) VALUES (NULL, '" + requestBody.name + "', '" + requestBody.email + "', '" + requestBody.password + "', '" + requestBody.gender + "', '" + requestBody.is_married + "', '" + requestBody.address + "');";
    connection.query(query, function (error, rows, fields) {
        if (error) {
            response.formatResponse('400', 'Failed', 'Insert Data Failed!', [], res)
        } else {
            response.formatResponse('201', 'Success', 'Insert Data Success!', rows, res)
        }
    });
};

exports.editCustomers = function (req, res) {
    var requestBody = req.body;
    var requestParam = req.params;
    var customer_id = requestParam.customer_id;

    var query = "UPDATE tbl_customer SET name = '" + requestBody.name + "' , email = '" + requestBody.email + "', password = '" + requestBody.password + "', gender = '" + requestBody.gender + "', is_married = '" + requestBody.is_married + "', address = '" + requestBody.address + "' WHERE id = '" + customer_id + "'; ";
    connection.query(query, function (error, rows, fields) {
        if (error) {
            response.formatResponse('400', 'Failed', 'Update Data Failed!', [], res)
        } else {
            response.formatResponse('200', 'Success', 'Update Data Success!', rows, res)
        }
    });
}

exports.deleteCustomers = function (req, res) {
    var requestParam = req.params;
    var customer_id = requestParam.customer_id;

    var query = "DELETE FROM tbl_customer WHERE id = '" + customer_id + "'; ";
    connection.query(query, function (error, rows, fields) {
        if (error) {
            response.formatResponse('400', 'Failed', 'Delete Data Failed!', [], res)
        } else {
            response.formatResponse('204', 'Success', 'Delete Data Success!', rows, res)
        }
    });
}