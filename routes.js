'use strict';

module.exports = function (app) {
    var controller = require('./controller');

    app.route('/')
        .get(controller.index);

    app.route('/customers')
        .get(controller.getCustomers);

    app.route('/customers')
        .post(controller.postCustomers);

    app.route('/customers/:customer_id')
        .put(controller.editCustomers);

    app.route('/customers/:customer_id')
        .delete(controller.deleteCustomers);
};