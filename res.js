'use strict';

exports.formatResponse = function (responseCode, response, message, values, res) {
    var data = {
        'status': {
            'code': responseCode,
            'response': response,
            'message': message
        },
        'result': values
    };
    res.json(data);
    res.end();
};