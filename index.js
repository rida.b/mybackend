var express = require("express");
var cors = require('cors');
var app = express();

app.use(cors());

app = express(),
    port = process.env.PORT || 3000,
    bodyParser = require('body-parser'),
    controller = require('./controller.js');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var routes = require('./routes');
routes(app);

app.listen(port);
console.log('Learn Node JS With Rida Bintara, RESTful API server started on: ' + port);